package com.skf.ble.viewModel

import android.bluetooth.BluetoothDevice
import android.content.DialogInterface
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModel


class SelectedVehicleViewModel : ViewModel() {

    val deviceList: MutableList<String> = ArrayList()

    fun createScanBLEDialog(view: View): AlertDialog {
        val dialogBuilder = AlertDialog.Builder(view.context)
        dialogBuilder.setTitle("SCANNING")
                .setCancelable(false)
        return dialogBuilder.create()
    }

    fun addBLEDialog(view: View, device: BluetoothDevice, listAdapter: ArrayAdapter<String>) {
        val dialogBuilder = AlertDialog.Builder(view.context)
        dialogBuilder.setTitle("ADD SENSOR")
                .setMessage("Sensor ${device.name} found.")
                .setCancelable(false)
                .setNegativeButton("Cancel", DialogInterface.OnClickListener {
                    dialog, id -> dialog.cancel()
                })
                .setPositiveButton("ADD", DialogInterface.OnClickListener {
                    dialog, id ->
                    Log.v("wtf", "adding device")
                    deviceList.add(device.name)
                    listAdapter.notifyDataSetChanged()
                })

        val alert = dialogBuilder.create()
        alert.show()
    }
}