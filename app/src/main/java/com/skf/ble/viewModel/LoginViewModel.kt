package com.skf.ble.viewModel

import android.view.View
import androidx.lifecycle.ViewModel
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import com.skf.ble.R

class LoginViewModel : ViewModel() {

    fun logIn(view : View) {
        view.findNavController().navigate(R.id.action_loginFragment_to_vehiclesFragment)
    }
}