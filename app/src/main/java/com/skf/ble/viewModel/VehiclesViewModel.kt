package com.skf.ble.viewModel

import android.content.Context
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import androidx.lifecycle.ViewModel
import androidx.navigation.findNavController
import com.skf.ble.R
import com.skf.ble.util.Vehicle
import com.skf.ble.util.VehicleAdapter

class VehiclesViewModel : ViewModel() {
    // TODO: Implement the ViewModel

    fun createVehiclesListAdapter(context : Context) : VehicleAdapter {

        val vehicles: ArrayList<Vehicle> = ArrayList()
        for(i in 0..5) {
            val vehicle = Vehicle("vehicle $i", i, R.drawable.logo)
            vehicles.add(vehicle)
        }

        return VehicleAdapter(context, vehicles)
    }

    fun vehiclePressed(view : View) {
        view.findNavController().navigate(R.id.action_vehiclesFragment_to_selectedVehicleFragment)
    }
}