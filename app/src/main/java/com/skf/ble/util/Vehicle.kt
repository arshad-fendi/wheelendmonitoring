package com.skf.ble.util

class Vehicle(
        val vehicleName: String,
        val vehicleSensorCount: Int,
        val vehicleIconId: Int
)