package com.skf.ble.util

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.skf.ble.R

class VehicleAdapter(context: Context, vehicles: ArrayList<Vehicle>) : ArrayAdapter<Vehicle>(context, 0, vehicles) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Check if the existing view is being reused, otherwise inflate the view
        var listItemView = convertView
        if (listItemView == null) {
            listItemView = LayoutInflater.from(context).inflate(
                    R.layout.vehicle_list_item, parent, false)
        }

        val currentVehicle: Vehicle? = getItem(position)

        val iconView = listItemView?.findViewById<View>(R.id.list_item_icon) as ImageView
        iconView.setImageResource(currentVehicle!!.vehicleIconId)

        val nameTextView = listItemView.findViewById<View>(R.id.list_item_title) as TextView
        nameTextView.text = currentVehicle.vehicleName

        val numberTextView = listItemView.findViewById<View>(R.id.list_item_number) as TextView
        numberTextView.text = currentVehicle.vehicleSensorCount.toString()

        return listItemView
    }
}