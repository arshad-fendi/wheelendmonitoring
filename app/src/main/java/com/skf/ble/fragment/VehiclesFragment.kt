package com.skf.ble.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.skf.ble.R
import com.skf.ble.databinding.FragmentVehiclesBinding
import com.skf.ble.util.Vehicle
import com.skf.ble.util.VehicleAdapter
import com.skf.ble.viewModel.VehiclesViewModel


class VehiclesFragment : Fragment() {

    companion object {
        fun newInstance() = VehiclesFragment()
    }

    private lateinit var viewModel: VehiclesViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentVehiclesBinding>(inflater, R.layout.fragment_vehicles, container, false)

        viewModel = ViewModelProvider(this).get(VehiclesViewModel::class.java)

        val adapter = viewModel.createVehiclesListAdapter(this.requireContext())
        binding.listView.adapter = adapter
        binding.listView.onItemClickListener = AdapterView.OnItemClickListener {
            adapterView: AdapterView<*>, view: View, i: Int, l: Long ->
            viewModel.vehiclePressed(view)

        }

        return binding.root
    }

}