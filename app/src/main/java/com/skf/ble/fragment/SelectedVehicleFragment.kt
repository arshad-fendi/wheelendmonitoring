package com.skf.ble.fragment

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.skf.ble.R
import com.skf.ble.databinding.FragmentSelectedVehicleBinding
import com.skf.ble.viewModel.SelectedVehicleViewModel
import java.util.*


class SelectedVehicleFragment : Fragment() {

    private var deviceFound = false
    lateinit var listAdapter: ArrayAdapter<String>

    private val bluetoothAdapter: BluetoothAdapter by lazy(LazyThreadSafetyMode.NONE) {
        val bluetoothManager = requireContext().getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothManager.adapter
    }

    private var mLeScanCallback: ScanCallback =
            object : ScanCallback() {
                override fun onScanResult(callbackType: Int, result: ScanResult?) {
                    super.onScanResult(callbackType, result)
                    Log.v("wtf", "result  = " + result)
                    val device = result?.device
                    if (device != null && device.name != null && device.name.startsWith("WEM")) {
                        Log.v("wtf", "found device  = " + device)

                        val uuidx = UUID.nameUUIDFromBytes(result.scanRecord?.bytes).toString()
                        Log.v("wtf", "uuidx = " + uuidx)

                        // Device found here
                        if(!deviceFound) {
                            Log.v("wtf", "showing dialog")
                            deviceFound = true
                            scanBLEDialog.dismiss()
                            viewModel.addBLEDialog(requireView(), device, listAdapter)
                        }
                    }
                }

                override fun onBatchScanResults(results: List<ScanResult?>?) {
                    super.onBatchScanResults(results)
                    Log.v("wtf", getString(R.string.found_ble_devices) + "result = $results")
                    scanBLEDialog.dismiss()
                }

                override fun onScanFailed(errorCode: Int) {
                    super.onScanFailed(errorCode)
                    Log.v("wtf", getString(R.string.ble_device_scan_failed) + "errorCode = $errorCode")
                    scanBLEDialog.dismiss()
                }
            }

    companion object {
        fun newInstance() = SelectedVehicleFragment()
    }

    private lateinit var viewModel: SelectedVehicleViewModel
    private lateinit var scanBLEDialog : AlertDialog

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewModel = ViewModelProvider(this).get(SelectedVehicleViewModel::class.java)

        val binding = DataBindingUtil.inflate<FragmentSelectedVehicleBinding>(inflater, R.layout.fragment_selected_vehicle, container, false)
        binding.fabAdd.setOnClickListener {
            scanBLE()
        }

        listAdapter = ArrayAdapter(requireContext(), android.R.layout.simple_dropdown_item_1line, viewModel.deviceList)
        binding.listView.adapter = listAdapter

        return binding.root
    }

    private fun scanBLE() {
        deviceFound = false
        scanBLEDialog = viewModel.createScanBLEDialog(requireView())
        scanBLEDialog.show()

        Handler().postDelayed({
            bluetoothAdapter?.bluetoothLeScanner?.stopScan(mLeScanCallback)
            scanBLEDialog.dismiss()
            Log.v("wtf", "stop scanning")
        }, 5000)
        Log.v("wtf", "start scanning")
        bluetoothAdapter?.bluetoothLeScanner?.startScan(mLeScanCallback)
    }

}